DEBUG = True
# Create dummy secrey key so we can use sessions
SECRET_KEY = '123456790'
# Create in-memory database
DATABASE_FILE = 'ariadne.sqlite'
SQLALCHEMY_DATABASE_URI = 'sqlite:///ariadne.sqlite'
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = True
