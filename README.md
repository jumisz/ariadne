# Ariadne

A scheduler tool for maintenance jobs.

## Features

- Run maintenance jobs
- Support for ansible, shell script
- User and Role based administration
- Audit Trail
- Report generator
- Pluggable output parsers
- Workflow via Pipelines
- Plugins
- Highly scalable
- Fast, lightweight

## Architecture

### Projects

### Inventories
Files containing lists of nodes (hosts, virtual servers, containers, network equipment).
These files are used by the tasks. A project can have several inventories and
an inventory can be used by many projects.

### Executor Nodes


### Tasks

### Missions
A mission is a group of tasks. 

### Users

### Roles

### Security
