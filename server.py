from app import manager
import logging
logger = logging.getLogger(__name__)
logger.info("Ariadne is Starting!")
if __name__ == '__main__':
    manager.run()
